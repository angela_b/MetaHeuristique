import numpy as np
from Particule import *

DEBUG = True


# Functions
def Jong_F1(array):
    sum = 0
    for xsi in array:
        sum += np.power(xsi, 2)
    return sum


def Jong_F2(array):
    X1 = array[0]
    X2 = array[1]
    return 100 * np.power((np.power(X1, 2) - X2), 2) + np.power((1 - X1), 2)


def Jong_F3(array):
    sum = 0
    for elem in array:
        sum += int(elem)
    return sum
def Goldstein(array):
    X1 = array[0]
    X2 = array[1]
    first_crochet = 1 + np.power(X1 + X2 + 1,2) * (19 - 14 * X1 + 3 * np.power(X1, 2) - 14 * X2 + 6 * X1 * X2 + 3 * np.power(X2, 2))
    second_crochet = 30 + np.power(2 * X1 - 3 * X2, 2) * (18 - 32 * X1 + 12 * np.power(X1, 2) + 48 * X2 - 36 * X1 * X2 + 27 * np.power(X2, 2))
    return int(first_crochet * second_crochet)

def Michaelwitcz(array):
    sum = 0
    i_value = 0
    for elem in array :
        sum += -np.sin(elem) * (np.sin((i_value + 1) * elem * elem / np.pi)) ** 20
        i_value += 1
       # return sum
    return float(int(1000 * sum) / 1000)

def Rosenbrock(array):
    sum = 0
    for i in range(len(array) - 1):
        xi = array[i]
        xi_1 = array[i + 1]
        sum = sum + (100 * np.power((xi_1 - np.power(xi, 2)), 2) + np.power((xi - 1), 2))
    return sum


def Zakharov(array):
    sum_1 = 0
    sum_2 = 0
    cpt = 1
    # Oui on peut tout faire en un mais c'est plus lisible pour debug
    for elem in array:
        sum_1 += np.power(elem, 2)
        sum_2 += 0.5 * cpt * elem
        cpt += 1
    return sum_1 + np.power(sum_2, 2) + np.power(sum_2, 4)


def Schwefel(array):
    sum = 0
    for elem in array :
        sum += -elem * np.sin(np.sqrt(abs(elem)))
    return sum
class Genetics:
    def __init__(self, NAME, NB_PARTICULE = 1000, TOLERANCE = 0.1):
        print("Initialisation Genetics de ", NAME)
        self.tolerance = TOLERANCE
        self.name = NAME
        self.nb_particule = NB_PARTICULE
        self.array = []

    # Definit les valeurs random de l'alea
    # Intialise le tableau de Particule
    def initialise_particule(self, low, high, SIZE = 20, n_particule = 1000):

        for i in range(n_particule): # Size est la taille du tableau particule
            self.array.append(Particule(low, high, SIZE))
        #self.print_particule(4)
# Calcul les scores de chaque particule et tri dans l'ordre de score
    def compute_score(self, function_score, GOAL):
        for particule in self.array:
            particule.score(function_score, GOAL)

        self.sort_by_score()
        #self.print_particule(4)

 # Classe du plus petit au plus grand
    def sort_by_score(self):
        self.array = sorted(self.array, key=lambda particule: particule.my_score)


    def print_score(self, SIZE = 10):
        for pos in range(SIZE):
            print(self.array[pos].my_score, " - ", end='')
        print("")

    def print_particule(self, NB_PARTICULE_TO_PRINT):
        print("=== BEGINNING ===")
        for particule in self.array[:NB_PARTICULE_TO_PRINT]:
            print(particule.array_point)
        print("=== END ===")

# Manipulation du tableau

    def regenerate_particuls(self, pos, low, high, SIZE):
      #  print("Avant rerandom ")
      #  self.print_particule(12)
        for position in range(pos, len(self.array)):
            self.array[position] = Particule(low, high, SIZE)
        #self.print_particule(12)
      #  print("Fin regenerate ")

    def mutate(self, pos_mutate, pos_stop):
        cpt = 0
        for position in range(pos_mutate, pos_stop):
            firstPart = self.array[position]
            secondPart = self.array[cpt] # Top  N des particules pour fusion

            for posx in range(len(firstPart.array_point)):
                firstPart.array_point[posx] = (firstPart.array_point[posx]  + secondPart.array_point[posx]) /  2#+ np.random.randint(0, abs(self.lowValue + 1))
            cpt += 1


    def genetic_launch(self, function_score ,low, high , SIZE, GOAL):
        n_iter = 0
        # Propriete du genetic
        nb_rerandom = self.nb_particule / 2
        nb_keep = self.nb_particule / 4
        nb_mutate = self.nb_particule / 4
        self.lowValue = low
        self.maxValue = high
        # Position lie
 # Low High size n_particule
        self.initialise_particule(low, high, SIZE, self.nb_particule)
        while True:
            # Calcul score pour chaque particule et trie le tableau
            self.compute_score(function_score, GOAL)
            if ((n_iter + 1) % 100 == 0):
                print("Best score ", self.array[0].my_score, " at iteration ", n_iter)
           #     self.print_particule(10)
            #    self.print_score(10)
            if (n_iter > 50000):
                return
                # print("Score 0 ",self.array[0].get_my_score())
            if ((self.array[0].my_score == 0) | (self.array[0].my_score  <= self.tolerance) & (self.array[0].my_score  >=   -self.tolerance)): # Plus exactment la difference entre la function score et
                # le GOAL est au centieme pres 0
                print("Solution trouve en ", n_iter, " iterations pour ",self.name)
                self.print_score(3)
                self.print_particule(3)
                break

            self.regenerate_particuls(int(nb_rerandom), low, high, SIZE)
            self.mutate(int(nb_keep), int(nb_mutate + nb_keep))
            n_iter += 1

        # Initialise le tableau de particules


# Les Tests

if DEBUG:
    genetic = Genetics("De Jong F1", 12)
    # Low High Size Goal
    genetic.genetic_launch(Jong_F1, -5.12, 5.12, 4, 0)
    print("Jong F1 terminé ")

    genetic = Genetics("De Jong F2", 12)
    genetic.genetic_launch(Jong_F2, -2.048, 2.048, 2, 0)
    print("Jong F2 terminé ")
    genetic = Genetics("De Jong F3", 100)
    genetic.genetic_launch(Jong_F3, -5.12, 5.12, 3, -5 * 3) # 5 * n
    print("Jong F3 terminé ")
    genetic = Genetics("Michael 2",1000)
    genetic.genetic_launch(Michaelwitcz, 0, np.pi + 0.01 , 2, -1.80) # 5 * n
    print("Michael 2 terminé ")
    genetic = Genetics("Michael 5 ",1000)
    genetic.genetic_launch(Michaelwitcz, 0, np.pi + 0.01, 5, -4.687) # 5 * n
    print("Michael 5 terminé ")
    genetic = Genetics("Michael 10 ",1000, 2)
    genetic.genetic_launch(Michaelwitcz, 0, np.pi + 0.01, 10, -9.68) # 5 * n
    print("Michael 10 terminé ")
    genetic = Genetics("GOldstein ", 1000, 2)
    genetic.genetic_launch(Goldstein, -2, 2, 2,3)
    print("Goldstein terminé ")
    genetic = Genetics("Rosenbrock ", 10)
    genetic.genetic_launch(Rosenbrock, -2.048, 2.048, 3 ,0)
    print("Rosenbrock terminé ")
    genetic = Genetics("Zakharov ", 1000)
    genetic.genetic_launch(Zakharov, -5, 10, 3, 0)
    print("Zakharov terminé ")
    genetic = Genetics("Schewtfel ", 100)
    genetic.genetic_launch(Schwefel, -500, 500, 4, -4 * 418.9829)  # -n * 418.9829
    print("Schwetfel terminé ")
