import numpy as np


class Particule:


    def __init__(self,low, high,  SIZE = 20):
        self.array_point = [] # tableau des points representant la particule
        for i in range(SIZE): # Low and upper bound
            self.array_point.append(np.random.uniform(low, high))
    #    print("Fin generation particule : ", self.array_point)
        self.my_score = np.inf

    def get_my_score(self):
        return self.my_score


    def score(self, function_score, GOAL):
        self.my_score = function_score(self.array_point)

        self.my_score = abs(GOAL - self.my_score)
        return self.my_score