# Generer N valeur aleatoires dans l'intervalle
# Nouvelle fonction de cout donnee dans le projet
# Nouvelle temperature dans le sujet
# Generer un nombre aleatoire et echanger avec un des trucs de l'intervalle
from random import shuffle, randint
from math import exp
import math
import random
import numpy as np
from numpy import *
TEMP_STOP = 1
class Meta:
    def __init__(self, size = 25):
        print("Initialized")

# Echange deux points dans l'array
    def swap(self,array, posA, posB):
        array[posA], array[posB] = array[posB], array[posA]

    # JONG F 1 Function
    # Jong F2
    def initialiseArray(self, size, intervalle):
        array = []
        for i in range(size):
            randomized = random.uniform(-intervalle, intervalle)
            array.append(randomized)
        return np.array(array)
    def initialise_goldstein(self):
        array = []
        for i in range(2):
            array.append(random.randint(-2, 2))
        return array

    def initialise_zakharov(self, size):
        array = []
        for i in range(size):
            array.append(random.randint(-5, 10))
        return array

    def initializzMichalewi(self, size):
        array = []
        for i in range(size):
            randomized = random.uniform(0, math.pi + 0.001)
            array.append(randomized)
        return np.array(array)

    def initialise_Schwefel(self, size):
        array = []
        for i in range(size):
            randomized = random.uniform(-500, 500)
            array.append(randomized)
        return np.array(array)

    ############  Distance ###############
    def computeDist_Jong(self, array):
        sum = 0
        for xsi in array:
            sum += np.power(xsi, 2)
        return sum
#F(x1,x2) = 100*(x1^2 - x2)^2 + (1 - x1)^2
    def computeDist_Jong_F2(self, array):
        X1 = array[0]
        X2 = array[1]
        return 100 * np.power((np.power(X1, 2) - X2),2) + np.power((1 - X1), 2)
    def computeDist_Jong_f3(self, array):
        sum = 0
        for elem in array :
            sum += int(elem)
        return sum

    def compute_Michael(self, array):
        sum = 0
        i_value = 0
        for elem in array :
            sum += -sin(elem) * (sin((i_value + 1) * elem * elem / pi)) ** 20
            i_value += 1
       # return sum
        return float(int(1000 * sum) / 1000)

# https://en.wikipedia.org/wiki/Test_functions_for_optimization pour la bonne fonction
    def compute_goldstein(self, array):
        X1 = array[0]
        X2 = array[1]
        first_crochet = 1 + np.power(X1 + X2 + 1,2) * (19 - 14 * X1 + 3 * np.power(X1, 2) - 14 * X2 + 6 * X1 * X2 + 3 * np.power(X2, 2))
        second_crochet = 30 + np.power(2 * X1 - 3 * X2, 2) * (18 - 32 * X1 + 12 * np.power(X1, 2) + 48 * X2 - 36 * X1 * X2 + 27 * np.power(X2, 2))
        return first_crochet * second_crochet

    def compute_Rosenbrock(self, array):
        sum = 0
        for i in range(len(array) - 1):
            xi = array[i]
            xi_1 = array[i + 1]
            sum = sum + (100 * np.power((xi_1 - np.power(xi, 2)), 2) + np.power((xi - 1), 2))
        return sum

    def compute_zakharov(self, array):
        sum_1 = 0
        sum_2 = 0
        cpt = 1
     # Oui on peut tout faire en un mais c'est plus lisible pour debug
        for elem in array:
            sum_1 += np.power(elem, 2)
            sum_2 += 0.5 * cpt * elem
            cpt += 1
        return sum_1 + np.power(sum_2, 2) + np.power(sum_2, 4)

    def compute_schwefel(self, array):
        sum = 0
        for elem in array :
            sum += -elem * sin(sqrt(abs(elem)))
        return sum
    def stopCondition(self,size):
        array = []
        for i in range(size):
            array.append(0)
        return array


    ''''
     Configuration initiale 
-> Temp initiale T 
-> (2) Perturbation de la configuration 
-> Solution meilleure acceptée / Slution pire accepté mais avec probabilité exp(-$\Delta E / T$) 
-> Equilibre thermodynamique ? Sinon retour perturbation (2) , ce retour est un pallier de température
-> Si oui, Systeme figé ? 
-> SI oui , Stop
-> Si non , On abaisse T retour etape (2)
'''

    def recuit_michalewicz(self, Temperature = 666666, SIZE = 10):
        print("LANCEMENT MICHAEL ")
        # Init param
        myArray = self.initializzMichalewi(SIZE)
        n_change = 0 # Stop count, number of run without change
        origin_dist = self.compute_Michael(myArray) # Value of the array before change
        next_dist = 0 # Value of the array after change
        self.changed = False # Is the distance change this turn
        currentTemp = Temperature # Temperature de l'iteration courante
        n_accepted = 0 # Nombre d'acceptation a ce palier de temp
        n_iteration = 0 # Compteur du nombre d'iteration pour s'arrêter

        while n_change < 3:
            n_iteration += 1
            # Perturbation
            randomValue = random.uniform(0, pi)  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE )  # Position a changer
            #print(randomPosition)
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.compute_Michael(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                currentTemp = currentTemp * 0.95
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("MichaelTemperature : ", currentTemp)
              #  print("Array : ", myArray)
                print("Distance ",origin_dist)
              #  print("Change ",n_change)

            if (SIZE == 2 and origin_dist == -1.80) :
                print("Solution trouve michael")
                break

            if (SIZE == 5 and origin_dist == -4.687) :
                print("Solution trouve michael")
                break

            if (SIZE == 10 and origin_dist == -9.67) :
                print("Solution trouve michael")
                break

        print("End of recuit strategy")
    def recuit_jong_f1(self, Temperature = 10, SIZE = 20):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT JOHN 1")

        # Init param
        ### De Jong  F 1 : ####
        myArray = self.initialiseArray(SIZE, 5.12)
        n_change = 0 # Stop count, number of run without change
        origin_dist = self.computeDist_Jong(myArray) # Value of the array before change
        next_dist = 0 # Value of the array after change
        self.changed = False # Is the distance change this turn
        currentTemp = Temperature # Temperature de l'iteration courante
        n_accepted = 0 # Nombre d'acceptation a ce palier de temp
        n_iteration = 0 # Compteur du nombre d'iteration pour s'arrêter
        # Generate solution for JONG
        comparedArraySolution = self.stopCondition(SIZE)
        # Recuit
        while n_change < 3:
            n_iteration += 1
            # Perturbation
            randomValue = float(int(100 * random.uniform(-5.12, 5.12)) / 100) # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE) # Position a changer
            memoryValue = myArray[randomPosition] # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue # Changement
            # Calcule nouvelle valeur
            next_dist = self.computeDist_Jong(myArray)
            if (next_dist <= origin_dist): # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else :
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0,1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted  += 1
                else :
                    #Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            #print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("JOHN F1 Temperature : ", currentTemp)
                print("Array : ", myArray)
            if (len(myArray[myArray != 0]) == 0):
                print("Solution trouve : 0 atteint")
                n_change = 3
                break
        print("End of recuit strategy")



# F(x1,x2) = 100*(x1^2 - x2)^2 + (1 - x1)^2
    # Min (1,1)
    def recuit_jong_f2(self, Temperature = 666666, SIZE = 2):
        print("LANCEMENT JOHN 2 ")
        # Generer N valeurs dans l'intervalle de definition :


        # Init param
        ### De Jong  F 1 : ####
        myArray = [-1.2, 1.2]#self.initialiseArray(SIZE, 2.048)
        n_change = 0 # Stop count, number of run without change
        origin_dist = self.computeDist_Jong_F2(myArray) # Value of the array before change
        next_dist = 0 # Value of the array after change
        self.changed = False # Is the distance change this turn
        currentTemp = Temperature # Temperature de l'iteration courante
        n_accepted = 0 # Nombre d'acceptation a ce palier de temp
        n_iteration = 0 # Compteur du nombre d'iteration pour s'arrêter
        # Generate solution for JONG
        comparedArraySolution = self.stopCondition(SIZE)
        # Recuit
        while n_change < 3:
            n_iteration += 1
            # Perturbation
            randomValue = random.uniform(-2.048, 2.048) # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE - 1) # Position a changer
            memoryValue = myArray[randomPosition] # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue # Changement
            # Calcule nouvelle valeur
            next_dist = self.computeDist_Jong_F2(myArray)
            if (next_dist <= origin_dist): # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else :
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0,1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted  += 1
                else :
                    #Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            #print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("JOHN F2 Temperature : ", currentTemp)
                print("Array : ", myArray)
                print("Distance :",origin_dist)
            if (int(100 * origin_dist) / 100 == 0): # au centieme de précision, peut être passe au dixieme
                print("Distance : ", origin_dist)
                print("Solution trouve : 0 atteint")
                n_change = 3
                break
        print("Final distance ",origin_dist)
        print("End of recuit strategy")

    def recuit_jong_f3(self, Temperature=666666, SIZE=5):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT JOHN 3")
        # Init param
        ### De Jong  F 1 : ####
        myArray = self.initialiseArray(SIZE, 5.12)
        n_change = 0  # Stop count, number of run without change
        origin_dist = self.computeDist_Jong_f3(myArray)  # Value of the array before change
        next_dist = 0  # Value of the array after change
        self.changed = False  # Is the distance change this turn
        currentTemp = Temperature  # Temperature de l'iteration courante
        n_accepted = 0  # Nombre d'acceptation a ce palier de temp
        n_iteration = 0  # Compteur du nombre d'iteration pour s'arrêter
        # Generate solution for JONG
        comparedArraySolution = self.stopCondition(SIZE)
        # Recuit
        stop_cond = 0 # Microvariation empeche la condition d'arret sinon
        while ((n_change < 3) & (stop_cond < 100000)) :
            n_iteration += 1
            stop_cond += 1
            # Perturbation
            randomValue = float(int(100 * random.uniform(-5.12, 5.12)) / 100)  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE)  # Position a changer
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.computeDist_Jong_f3(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                print("N_change", n_change)
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("JOHN F3 Temperature : ", currentTemp)
                print("Distance : ",origin_dist)
                print("Fonction objectif : ", -5 * (SIZE))

                print(myArray)
            if (origin_dist == (-5 * SIZE)):
                print(myArray)
                print("Solution trouve : 0 atteint")
                n_change = 3
                break
        print("End of recuit strategy")

    def recuit_goldstein(self, TEMPERATURE = 10, SIZE = 2):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT Goldstein ")
        # Init param
        myArray = self.initialise_goldstein()
        print(myArray)
        n_change = 0  # Stop count, number of run without change
        origin_dist = self.compute_goldstein(myArray)  # Value of the array before change
        next_dist = 0  # Value of the array after change
        self.changed = False  # Is the distance change this turn
        currentTemp = TEMPERATURE  # Temperature de l'iteration courante
        n_accepted = 0  # Nombre d'acceptation a ce palier de temp
        n_iteration = 0  # Compteur du nombre d'iteration pour s'arrêter
        # Generate solution for JONG
        comparedArraySolution = self.stopCondition(SIZE)
        # Recuit
        stop_cond = 0  # Microvariation empeche la condition d'arret sinon
        while ((n_change < 3) & (stop_cond < 100000)):
            n_iteration += 1
            stop_cond += 1
            # Perturbation
            randomValue = random.randint(-2, 2)  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE)  # Position a changer
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.compute_goldstein(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                print("N_change", n_change)
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("JOHN F3 Temperature : ", currentTemp)
                print("Distance : ", origin_dist)

                print(myArray)
            if (origin_dist == 3):
                print(myArray)
                print("Solution trouve : minimum atteint")
                n_change = 3
                break
        print("End of recuit strategy")

    def recuit_Rosenbrock(self, TEMPERATURE = 10, SIZE = 20):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT Rosenbrock ")
        # Init param
        myArray = self.initialiseArray(SIZE, 2.048)#initialise_goldstein()
        print(myArray)
        n_change = 0  # Stop count, number of run without change
        origin_dist = self.compute_Rosenbrock(myArray) # Value of the array before change
        next_dist = 0  # Value of the array after change
        self.changed = False  # Is the distance change this turn
        currentTemp = TEMPERATURE  # Temperature de l'iteration courante
        n_accepted = 0  # Nombre d'acceptation a ce palier de temp
        n_iteration = 0  # Compteur du nombre d'iteration pour s'arrêter
        # Recuit
        stop_cond = 0  # Microvariation empeche la condition d'arret sinon
        while ((n_change < 3) & (stop_cond < 100000)):
            n_iteration += 1
            stop_cond += 1
            # Perturbation
            randomValue = random.uniform(-2.048, 2.048)  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE)  # Position a changer
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.compute_Rosenbrock(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                print("N_change", n_change)
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("Rosenbrock Temperature : ", currentTemp)
                print("Distance : ", origin_dist)

                print(myArray)
            if (origin_dist < 0.1):
                print(myArray)
                print(origin_dist)
                print("Solution trouve : minimum casiment atteint")
                n_change = 3
                break
        print("End of recuit strategy")

    def recuit_zakharov(self, TEMPERATURE=10, SIZE=20):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT Zakharov ")
        # Init param
        myArray = self.initialise_zakharov(SIZE)
        print(myArray)
        n_change = 0  # Stop count, number of run without change
        origin_dist = self.compute_zakharov(myArray)  # Value of the array before change
        next_dist = 0  # Value of the array after change
        self.changed = False  # Is the distance change this turn
        currentTemp = TEMPERATURE  # Temperature de l'iteration courante
        n_accepted = 0  # Nombre d'acceptation a ce palier de temp
        n_iteration = 0  # Compteur du nombre d'iteration pour s'arrêter
        # Recuit
        stop_cond = 0  # Microvariation empeche la condition d'arret sinon
        while ((n_change < 3) & (stop_cond < 100000)):
            n_iteration += 1
            stop_cond += 1
            # Perturbation
            randomValue = random.randint(-5, 10)  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE)  # Position a changer
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.compute_zakharov(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                print("N_change", n_change)
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("Zakharov Temperature : ", currentTemp)
                print("Distance : ", origin_dist)

                print(myArray)
            if (origin_dist < 0.1):
                print(myArray)
                print(origin_dist)
                print("Solution trouve : minimum casiment atteint")
                n_change = 3
                break
        print("End of recuit strategy")

    def recuit_Schwefel(self, TEMPERATURE=10, SIZE=20):
        # Generer N valeurs dans l'intervalle de definition :
        print("LANCEMENT Schnitzel ")
        # Init param
        myArray = self.initialise_Schwefel(SIZE)
        print(myArray)
        n_change = 0  # Stop count, number of run without change
        origin_dist = self.compute_schwefel(myArray)  # Value of the array before change
        next_dist = 0  # Value of the array after change
        self.changed = False  # Is the distance change this turn
        currentTemp = TEMPERATURE  # Temperature de l'iteration courante
        n_accepted = 0  # Nombre d'acceptation a ce palier de temp
        n_iteration = 0  # Compteur du nombre d'iteration pour s'arrêter
        # Recuit
        stop_cond = 0  # Microvariation empeche la condition d'arret sinon
        while ((n_change < 3) & (stop_cond < 100000)):
            n_iteration += 1
            stop_cond += 1
            # Perturbation
            randomValue = float(int( 10000 * random.uniform(-500, 500)) / 10000 )  # Valeur aleatoire a echange
            randomPosition = random.randint(0, SIZE)  # Position a changer
            memoryValue = myArray[randomPosition]  # Sauvegarde pour annule le changement
            myArray[randomPosition] = randomValue  # Changement
            # Calcule nouvelle valeur
            next_dist = self.compute_schwefel(myArray)
            if (next_dist <= origin_dist):  # Si amelioration alors sauvegarde
                origin_dist = next_dist
                n_accepted += 1
                self.changed = True
            else:
                valueDelta = -((next_dist - origin_dist) / currentTemp)
                expDelta = exp(valueDelta)
                randomTest = randint(0, 1000000)  # Genere la valeur qui permettra de dire si
                #  on accepte une valeure meme fausse
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    self.changed = True
                    origin_dist = next_dist
                    n_accepted += 1
                else:
                    # Changement refuse, retour sur erreur
                    myArray[randomPosition] = memoryValue

            # print("Verification : n_change ",n_change, " n_iteration ", n_iteration, " Distance ",origin_dist, " Temp ", currentTemp)
            # print("Change ",n_change)
            if ((n_iteration >= 100 * SIZE) | (n_accepted >= 12 * SIZE)):
                print("N_change", n_change)
                currentTemp = currentTemp * 0.9
                n_iteration = 0
                n_accepted = 0
                if self.changed:
                    n_change = 0
                else:
                    n_change += 1
                self.changed = False
                print("Schnitzel Temperature : ", currentTemp)
                print("Distance : ", origin_dist)

                print("Fonction objectif ", -SIZE * 418.9829)
                print(myArray)
            if (abs((-SIZE * 418.9829 - origin_dist)) <1): # Accepte distance de 1
                print(myArray)
                print("Distance atteinte ",origin_dist)
                print("Solution trouve : minimum casiment atteint")
                n_change = 3
                break
        print("End of recuit strategy")
meta = Meta()
# meta.recuit_jong_f1()
# meta.recuit_jong_f2()
# meta.recuit_jong_f3()
# meta.recuit_goldstein()
# meta.recuit_michalewicz(10, 2)
# meta.recuit_michalewicz(10, 5)
# meta.recuit_michalewicz(10, 10)
# Rosenbrock Optimum : [ 1, 1, ..., 1]
# meta.recuit_Rosenbrock(100, 3)
# Zakharov optimum : [0 ...0]
# meta.recuit_zakharov(10, 5)
# Schnitzel optimum , flingue [420,9867, ..... 420.9687]
meta.recuit_Schwefel()