from random import shuffle, randint
from math import exp
import networkx as nx
import matplotlib.pyplot as plt

TEMP_STOP = 1
printer = []
class Meta:
    def __init__(self, size = 25):

        self.current_figure = 0

        self.listLink = [] # Contient n cases, chaque case est une liste chainee vers la suite de sommets liés
        self.listSommet = []
        for i in range(size):
            self.listLink.append(i)
            self.listLink[i] = []
            self.listSommet.append(i)

# Parse le fichier et cree la liste de liens entre sommets
    def parseFile(self):
        fd = open("Recuit/input.txt", "r")
        position = 0
        line = fd.read()
        print("Split ", line.splitlines())
        for sommet in line.splitlines():
            for elem in sommet.split(","):
                self.listLink[position].append(elem)
            position += 1
        print("List link ", self.listLink)

    def plotGraph(self):
        graph = nx.Graph()
        plt.figure(self.current_figure)
        self.current_figure += 1
        positions = {}
        i = 4
        for node_list in self.array:
            j = 0
            for node in node_list:
                positions[int(node)] = (j, i)
                graph.add_node(int(node))
                for neighbour in self.listLink[int(node)]:
                    graph.add_edge(int(node), int(neighbour))
                j += 1
            i -= 1

    #    print("positions", positions)

        nx.draw_networkx(graph, pos=positions)


# Echange deux points dans l'array
    def swap(self, firstPosX, firstPosY, secondPosX, secondPosy):
        tmp = self.array[firstPosX][firstPosY]
        self.array[firstPosX][firstPosY] = self.array[secondPosX][secondPosy]
        self.array[secondPosX][secondPosy] = tmp


# Fonction annexe al a distance de manhattan
    def computeDist(self, currentSommetX, currentSommetY, destination):
        # Trouver la position dans l'array de notre destination
        posx = 0
        found = False
        posy = 0
        for row in self.array:
            posy = 0
            for column in row:
                if column == destination :
                    found = True
                    break
                posy += 1
            if found:
                break
            posx +=1
        #print("[COMPUTE_DIST] Posx found ",posx, " Posy found ",posy)
        return 5 *  (abs(currentSommetY - posy) + abs(currentSommetX - posx))


# Calcul la disntace entre un point et tout ceux objectifs contenus dans listDestination
    def manhattan_distance(self, posx, posy, valueDistance = 5):
        currentSommet = self.array[posx][posy] # sommet courant
        listDestination = self.listLink[currentSommet] # Recupere la liste des sommets pour lesquels on calculera la distance
        # vis a vis du point courant
        #print("Destination ",listDestination)

        distance_sum = 0
        for destination in listDestination:
            distance = self.computeDist(posx, posy, int(destination))
          #  print("[MANHATTAN] Distance entre ",currentSommet, " et ", destination, " est de ",distance)
            distance_sum += distance
       # print("[MANHATTAN] Distance manhattan du sommet ",currentSommet, " " ,distance_sum)
        return distance_sum



# Unit value , Value par defaut d'un lien
    # Parcourt tout les sommets et calculs leur distance de manhattan par rapport
    # a leur objectif et en renvoie la somme
    def compute_manhattan(self, unitValue = 5):
        distanceSum = 0
        for i in range(5):
            for j in range(5):
                distanceSum += self.manhattan_distance(i,j, unitValue)
    #    print("[COMPUTEManhattan] Distance of the board ",distanceSum)
        return distanceSum / 2 # Diviser par deux car on compte deux fois les dist


    def generate_array(self):
        self.array = []
        for i in range(5):
            self.array.append([])
            for j in range(5):
                self.array[i].append(self.listSommet[i * 5 + j])


    ''''
     Configuration initiale 
-> Temp initiale T 
-> (2) Perturbation de la configuration 
-> Solution meilleure acceptée / Slution pire accepté mais avec probabilité exp(-$\Delta E / T$) 
-> Equilibre thermodynamique ? Sinon retour perturbation (2) , ce retour est un pallier de température
-> Si oui, Systeme figé ? 
-> SI oui , Stop
-> Si non , On abaisse T retour etape (2)
'''


    def recuit(self, Temperature = 10):
        # Preproc
        shuffle(self.listSommet) # Melange les sommets pour une config initiale
     #   print("Shuffled ",self.listSommet)
        self.generate_array()
        #self.plotGraph()
       # print(self.array)
        # Variables
        currentTemperature = Temperature
        currentDistance = self.compute_manhattan()   # Premiere value initialise, valeur de la board en distance
        n_iteration =  0
        n_accepted = 0
        change = True # Dit si changement
        n_change = 0 # COmpte le nombre d'iteration sans changement
        n_sommet = 25
        n_tot = 0
        while n_change < 3 :
            n_tot += 1
            #print("Array ",self.array)

           # if n_iteration % 5000 == 0:
                #print("Distance courante ",currentDistance)
                #self.plotGraph()
            #print("[RECUIT] Current temperature ",currentTemperature)
            n_iteration += 1
            self.posXSommet1 = randint(0, 4)
            self.posYSommet1 = randint(0, 4)
            while True:  # Ne pas tirer deux fois le meme sommet
                self.posXSommet2 = randint(0, 4)
                self.posYSommet2 = randint(0, 4)
                if (self.posXSommet1 != self.posXSommet2 | self.posYSommet1 !=  self.posYSommet2):
                    break
            #print("[RECUIT] Array avant swap ",self.array)
            self.swap(self.posXSommet1, self.posYSommet1, self.posXSommet2, self.posYSommet2)
            #print("[RECUIT] Array apres swap ",self.array)
            new_dist = self.compute_manhattan()
        #    print("[RECUIT] Distance avant swap", currentDistance)
            #print("[RECUIT] Distance after swap ",new_dist)
            # Cas d'acceptation
            if (new_dist <= currentDistance):
                currentDistance = new_dist
                change = True
                n_accepted += 1
            else:
                valueDelta = -((new_dist - currentDistance) / currentTemperature)
              #  print("Value delta ",valueDelta)
                expDelta =  exp(valueDelta)
             #   print("[RECUIT] Proba acceptation ", expDelta)
                randomTest = randint(0, 1000000) # Genere la valeur qui permettra de dire si on accepte une valeure meme fausse
              #  print("[RECUIT] Compare ",randomTest, " and ",int(expDelta*1000000))
                if (randomTest <= expDelta * 1000000):
                    # Cas acceptation malgre proba
                    change = True
                    currentDistance = new_dist
                else :
                    self.swap(self.posXSommet2, self.posYSommet2, self.posXSommet1, self.posYSommet1) # Refuse swap
                   # print("Verif annule swap ", self.array)


           # print("Change ",n_change)
            if ((n_iteration >= 100 * n_sommet) |  (n_accepted >= 12 * n_sommet)):  # |
                currentTemperature = currentTemperature * 0.9
             #   print("Derniere distance : ",currentDistance, " Stop a cause de ",n_iteration, " ou ",n_accepted)
                n_iteration = 0
                n_accepted = 0
                if change:
                    n_change = 0
                else:
                    n_change += 1
                change = False
                print("Temperature : ",currentTemperature)
            if (n_tot > 100000):
                print('boom')
                return n_iteration
            if (currentDistance == 200):
                print("FOUND : Distance 200 found")
                print("N Iteration : ",n_tot)

                #self.plotGraph()
                return n_tot
        return n_tot
for i in range(20):
    print(i)
    meta = Meta()
    meta.parseFile()
    printer.append(meta.recuit())

plt.plot(printer)
#meta.recuit()
#meta.compute_manhattan()
plt.show()
